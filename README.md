# HP SCDS ML/AI talks 2022

This is the repository that contains the materials for the HP SCDS ML/AI talks 2022.

## Installation

It is recommended to create an environment for executing the notebooks and scripts of the talks. If you are using virtualenv, you must just execute

    virtualenv python3.9 hpscds_ml_talks

However, we recommend you to use `conda`

    conda create -n hpscds_ml_talks python=3.9

where `hpscds_ml_talks` can be whatever name you prefer for your environment.

Then, once the environment is activated, you can install the mandatory modules using the requirements.txt file

    pip install -r requirements.txt

## Sessions

Find here the sessions for the previous talks (external attendees please use the YouTube links, the other are internal resources that only work for employees):

- Session 1 (employees only) - Introduction to Deep Learning: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-1), [MS Stream](https://web.microsoftstream.com/video/53b5bdf4-92e0-449e-832e-a221e6af1a76?channelId=1e813230-38ac-4e22-bf7f-39767812befc)
- Session 2 (employees only) - Demo Session: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-2), [MS Stream](https://web.microsoftstream.com/video/bae7eaac-9266-4988-bd5e-2dc2a86e9021?channelId=1e813230-38ac-4e22-bf7f-39767812befc)
- Session 3 (all) - Basic Math: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-3), [MS Stream](https://web.microsoftstream.com/video/3f8f008a-0cb2-4ac6-bc8a-cdcc88473c8a?channelId=1e813230-38ac-4e22-bf7f-39767812befc), [YouTube](https://www.youtube.com/watch?v=WBWa9HnjAJE)
- Session 4 (all) - Machine Learning Basics: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-4), [MS Stream](https://web.microsoftstream.com/video/61cd71f6-b936-4870-b43b-a8682ba9377f?channelId=1e813230-38ac-4e22-bf7f-39767812befc), [YouTube](https://www.youtube.com/watch?v=-uQtiOARNz4)
- Session 5 (all) - Multilayer Perceptron: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-5), [MS Stream](https://web.microsoftstream.com/video/e2721c1e-9078-4b97-b8bd-746738579244?channelId=1e813230-38ac-4e22-bf7f-39767812befc), [YouTube](https://www.youtube.com/watch?v=B1sB9sq3e7I)
- Session 6 (all) - Optimization on the House Prices problem: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-6), [MS Stream](https://web.microsoftstream.com/video/5a54967d-afcd-4dc4-8984-8acc512b1580?channelId=1e813230-38ac-4e22-bf7f-39767812befc), [YouTube](https://www.youtube.com/watch?v=s8lqOI42obI)
- Session 7 (all) - Convolutional Neural Networks: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-7), [MS Stream](https://web.microsoftstream.com/video/8d7fc3df-60da-4db7-ab5b-54096043565d?channelId=1e813230-38ac-4e22-bf7f-39767812befc), [YouTube](https://www.youtube.com/watch?v=WndP4tvZrmc)
- Session 8 (all) - Semantic Segmentation with Modern Convolutional Neural Networks: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-8), [MS Stream](https://web.microsoftstream.com/video/01b32929-9c66-4219-bb19-eacbd60efe0e?channelId=1e813230-38ac-4e22-bf7f-39767812befc), [YouTube](https://www.youtube.com/watch?v=wgwOCFwqSfE)
- Session 9 (all) - Recurrent Neural Networks: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-9), [MS Stream](https://web.microsoftstream.com/video/281b7604-8283-4691-af8c-deec34938979?channelId=1e813230-38ac-4e22-bf7f-39767812befc), [YouTube](https://www.youtube.com/watch?v=dYOKSAu80XU)
- Session 10 (all) - Attention Mechanisms and Transformers: [Alejandreta](https://alejandreta.leo.rd.hpicorp.net/media/ml-talks-10), [MS Stream](https://web.microsoftstream.com/video/7343fde8-f9f8-44b3-879d-98c4a6b65486?channelId=1e813230-38ac-4e22-bf7f-39767812befc), [YouTube](https://www.youtube.com/watch?v=oLYmVvy5NKw)
